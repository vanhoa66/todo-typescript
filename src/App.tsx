import React from 'react';
import { useSelector } from 'react-redux';

import './App.css';
import Header from './components/Header';
import DeleteListModal from './components/DeleteListModal';
import EditListModal from './components/EditListModal';
import Notification from './components/Notification';
import CreateNewList from './components/CreateNewList';
import Lists from './components/Lists';

import { RootState } from './store';

function App() {
    const notificationMsg = useSelector((state: RootState) => state.notification.message);
    const listIdToDelete = useSelector((state: RootState) => state.list.listIdToDelete);
    const listToEdit = useSelector((state: RootState) => state.list.listToEdit);

    return (
    <div className="App">
        <Header title="Todo App" subtitle="Create todo task" />
        <div className="container px-5">
            <div className="columns">
                <div className="column is-6">
                    <CreateNewList />
                    <Lists />
                </div>
            </div>
        </div>

        <Notification msg={notificationMsg} />
        {listIdToDelete && <DeleteListModal listId={listIdToDelete} />}
        {listToEdit && <EditListModal list={listToEdit} />}
    </div>
  )
}

export default App;
