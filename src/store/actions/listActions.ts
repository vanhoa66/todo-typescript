import { List, ListsAction, ADD_LIST, GET_LISTS, GET_LIST_BY_ID, SET_LISTID_TO_DELETE, SET_LIST_TO_EDIT, DELETE_LIST, UPDATE_LIST} from "../types";

export const addList = (list: List): ListsAction => {
    return {
        type: ADD_LIST,
        payload: list
    }
}

export const getLists = (): ListsAction => {
    return {
        type: GET_LISTS
    }
}

export const getListById = (id: string): ListsAction => {
    return {
        type: GET_LIST_BY_ID,
        payload: id
    }
}

export const setListIdToDelete = (id: string): ListsAction => {
    return {
        type: SET_LISTID_TO_DELETE,
        payload: id
    }
}

export const setListToEdit = (id: string): ListsAction => {
    return {
        type: SET_LIST_TO_EDIT,
        payload: id
    }
}

export const deleteList = (id: string): ListsAction => {
    return {
        type: DELETE_LIST,
        payload: id
    }
}

export const updateList = (id: string, name: string): ListsAction => {
    return {
        type: UPDATE_LIST,
        payload: {
            id,
            name
        }
    }
}



